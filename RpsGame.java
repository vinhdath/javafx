import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rand;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.rand = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound(String choice) {
        int cpuNumber = rand.nextInt(3);

        String cpuChoice = null;
        switch (cpuNumber) {
        case 0:
            cpuChoice = "rock";
            break;
        case 1:
            cpuChoice = "scissors";
            break;
        case 2:
            cpuChoice = "paper";
            break;
        default:
            cpuChoice = null;
        }

        if(choice.toLowerCase().equals(cpuChoice)){
    
            return playerTies(cpuChoice);
    
        }
        else if (choice.toLowerCase().equals("rock")) {
            return playerPlayRock(cpuChoice);
        }
        else if (choice.toLowerCase().equals("scissors")) {
            return playerPlayScissors(cpuChoice);
        }
        else if (choice.toLowerCase().equals("paper")) {
            return playerPlayPaper(cpuChoice);
        }
        else {
            return "Something Wrong here";
        }


    }

    @Override
    public String toString() {
        return "RpsGame [losses=" + this.losses  + ", ties=" + this.ties + ", wins=" + this.wins + "]";
    }

    public String playerTies(String cpuChoice) {

        this.ties++;
        return "Computer plays " + cpuChoice + " and this is a tie game";
    }

    public String playerPlayRock(String cpuChoice) {

        if (cpuChoice.equals("scissors")) {
            this.wins++;
            return "Computer plays scissors and You won";
        } else if (cpuChoice.equals("paper")) {
            this.losses++;
            return "Computer plays paper and You lose";

        } else {
            return "Something wrong happened";
        }
    }

    public String playerPlayScissors(String cpuChoice) {
        if (cpuChoice.equals("rock")) {
            this.losses++;
            return "Computer plays rock and You lose";
        } else if (cpuChoice.equals("paper")) {
            this.wins++;
            return "Computer plays paper and You won";

        }else{
            return "Something wrong happened";
        } 
    }
    

    public String playerPlayPaper(String cpuChoice) {
        if (cpuChoice.equals("rock")) {
            this.wins++;
            return "Computer plays rock and You won";
        } else if (cpuChoice.equals("scissors")) {
            this.losses++;
            return "Computer plays scissors and You lose";
        }
        else{
            return "Something wrong happened";
        }

    }
}