import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class Buttons extends HBox {
    Button btn1;
    Button btn2;
    Button btn3;

    public Buttons() {
        this.btn1 = new Button("Rock");
        this.btn2 = new Button("Scissors");
        this.btn3 = new Button("Paper");

        this.getChildren().addAll(btn1, btn2, btn3);
    }

}
