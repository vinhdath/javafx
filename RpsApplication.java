
import org.w3c.dom.NodeList;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {

    @Override
    public void start(Stage stage) {
        // container and layout
        Group root = new Group();

        // Add buttons and textfields
        Buttons buttons = new Buttons();
        Texts textFields = new Texts();

        VBox vB = new VBox();
        vB.getChildren().addAll(buttons, textFields);
        root.getChildren().add(vB);

        // Connect backend code
        RpsGame rps = new RpsGame();
        RpsChoice rpsChoice;

        // Set handlers
        buttons.btn1.setOnAction(new RpsChoice(textFields, buttons.btn1.getText(), rps));
        buttons.btn2.setOnAction(new RpsChoice(textFields, buttons.btn2.getText(), rps));
        buttons.btn3.setOnAction(new RpsChoice(textFields, buttons.btn3.getText(), rps));

        // scene is associated with container, dimensions
        Scene scene = new Scene(root, 951, 300);
        scene.setFill(Color.BLACK);

        // associate scene to stage and show
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
