import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class Texts extends HBox {
  TextField tf1;
  TextField tf2;
  TextField tf3;
  TextField tf4;

  public Texts() {
    tf1 = new TextField("Welcome!");
    tf2 = new TextField("wins: 0");
    tf3 = new TextField("losses: 0");
    tf4 = new TextField("ties: 0");

    this.getChildren().addAll(tf1, tf2, tf3, tf4);

    // Set width for textFileds
    tf1.setPrefWidth(250);
    tf2.setPrefWidth(200);
    tf3.setPrefWidth(200);
    tf4.setPrefWidth(200);
  }
}
