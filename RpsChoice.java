import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RpsChoice implements EventHandler<ActionEvent> {
    Texts fields;
    String playerChoice;
    RpsGame game;

    public RpsChoice(Texts fields, String playerChoice, RpsGame game) {
        this.fields = fields;
        this.playerChoice = playerChoice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e) {
        // Update text fields for each game played
        String result = game.playRound(playerChoice);
        fields.tf1.setText(result);
        fields.tf2.setText("win: " + game.getWins());
        fields.tf3.setText("losses: " + game.getLosses());
        fields.tf4.setText("ties: " + game.getTies());
    }
}
